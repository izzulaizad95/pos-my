import React from "react";

export const Footer = (props: any) => {
  const { options, style } = props;
  let single: boolean = false;
  let multiple: boolean = true;

  if (options?.length > 2) {
    multiple = true;
  }

  if (options?.length === 1) {
    single = true;
  }

  return (
    <div className="footer" id="footer" style={{ ...style }}>
      {/* {
        <div className="footer-btn">
          {options.map((el, index) => (
            <Button
              startIcon={el?.startIcon}
              key={index}
              type="submit"
              color={el.color}
              style={{
                padding: "5px",
                minWidth:
                  options.length > 2 ? (multiple ? "85px" : "50px") : "126px",
              }}
              variant="contained"
              onClick={el.onClick}
              disabled={el.disabled}
              {...el.props}
            >
              {el.name}
            </Button>
          ))}
        </div>
      } */}
    </div>
  );
};
