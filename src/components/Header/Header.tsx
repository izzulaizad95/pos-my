import { IconButton, Menu, MenuItem } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import { useTheme } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import React from "react";
import { useHistory } from "react-router";

export const Header = (props: any) => {
  const {
    title,
    rightIcon,
    rightInfo,
    rightMenu,
    onClick,
    menuData,
    anchorEl,
    handleMenuClose,
  } = props;

  const theme = useTheme();
  const history = useHistory();

  return (
    <div className="main-header-fix">
      <AppBar position={"fixed"} className={`mobile-header fix-responsive`}>
        <Toolbar>
          <div className="domain-container">
            <IconButton
              className="first-btn"
              onClick={onClick ?? (() => history.goBack())}
            >
              <KeyboardArrowLeftIcon />
            </IconButton>
            <div className="domain-session">
              <div className="domain">{title}</div>
            </div>
            {rightIcon && rightIcon}
            {rightInfo && rightInfo}

            {rightMenu && (
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={Boolean(anchorEl)}
                onClose={handleMenuClose}
              >
                {menuData?.map((data) => {
                  return (
                    <MenuItem>{`${data?.name} x${data?.quantity}`}</MenuItem>
                  );
                })}
              </Menu>
            )}
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
};
