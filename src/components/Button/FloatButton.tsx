import { Fab } from "@material-ui/core";
import React from "react";

export const FloatButton = (props: any) => {
  const { options, onClick, label, icon, className, style, color } = props;
  let single: boolean = false;
  let multiple: boolean = true;

  if (options?.length > 2) {
    multiple = true;
  }

  if (options?.length === 1) {
    single = true;
  }

  return (
    <Fab
      variant="extended"
      size="small"
      color={color ?? "primary"}
      aria-label="add"
      className={className ?? "float-btn"}
      style={style ?? null}
      onClick={onClick}
    >
      {icon ?? label}
    </Fab>
  );
};
