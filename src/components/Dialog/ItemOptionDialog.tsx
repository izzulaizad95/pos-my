import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
} from "@material-ui/core";
import * as React from "react";
import NumberFormat from "react-number-format";

export const ItemOptionDialog = (props: any) => {
  const {
    openItemDialog,
    setOpenItemDialog,
    selectedItem,
    size,
    setSize,
    cheese,
    setCheese,
    quantity,
    setQuantity,
    onSubmit,
  } = props;
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog
        open={openItemDialog}
        onClose={() => setOpenItemDialog(false)}
        className="item-dialog"
      >
        <DialogTitle style={{ padding: "16px 24px 0px" }}>
          {selectedItem?.name}
        </DialogTitle>
        <DialogContent>
          <Grid container>
            <Grid item xs={3} style={{ alignSelf: "start", marginTop: "10px" }}>
              Size
            </Grid>
            <Grid item xs={9} style={{ textAlign: "right" }}>
              <FormControl>
                <RadioGroup
                  row
                  aria-labelledby="demo-row-radio-buttons-group-label"
                  name="row-radio-buttons-group"
                  onChange={(event) => {
                    setSize(event?.target?.value);
                  }}
                >
                  <FormControlLabel
                    value="small"
                    control={<Radio />}
                    label="Small"
                  />
                  <FormControlLabel
                    value="medium"
                    control={<Radio />}
                    label="Medium"
                  />
                  <FormControlLabel
                    value="large"
                    control={<Radio />}
                    label="Large"
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid item xs={6} style={{ alignSelf: "center" }}>
              Extra Cheese?
            </Grid>
            <Grid item xs={6} style={{ textAlign: "right" }}>
              <Checkbox
                style={{ fontWeight: "bold", marginRight: "0px" }}
                name={"cheese"}
                color="primary"
                onClick={(e) => {
                  setCheese(!cheese);
                }}
              />
            </Grid>
            <Grid item xs={9} style={{ alignSelf: "center" }}>
              Quantity
            </Grid>
            <Grid item xs={3}>
              <NumberFormat
                name="quantity"
                placeholder="e.g. 1"
                customInput={TextField}
                variant="outlined"
                margin="dense"
                autoComplete="off"
                allowNegative={false}
                multiline={true}
                style={{ margin: "0px", float: "right", paddingRight: "12px" }}
                className="number-input"
                defaultValue={0}
                inputProps={{ inputmode: "numeric", pattern: "[0-9]*" }}
                onValueChange={(e) => {
                  const floatValue = e?.floatValue;
                  setQuantity(floatValue);
                }}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions className="button-input">
          <Button variant="contained" onClick={() => setOpenItemDialog(false)}>
            Cancel
          </Button>
          <Button variant="contained" onClick={() => onSubmit()}>
            Add To Cart
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
