import React from "react";
import { render } from "react-dom";
import App from "./containers/App/App";
import "./index.css";

// const rootEl = document.getElementById("root") as HTMLElement;
// const root = createRoot(rootEl);

// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();

document.addEventListener("DOMContentLoaded", () => {
  render(<App />, document.body.appendChild(document.createElement("div")));
});
