import React from "react";
import { Redirect, Route, Switch } from "react-router";
import { Checkout } from "../../modules/Checkout";
import { Home } from "../../modules/Home";
import NotFound from "../../modules/NotFound";
import { Order } from "../../modules/Order";

const Routes = () => {
  return (
    // <Header />
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/home">
        <Home />
      </Route>
      <Route exact path="/:mode/order">
        <Order />
      </Route>
      <Route exact path="/:mode/order/checkout">
        <Checkout />
      </Route>
      <Route exact path="/not-found">
        <NotFound />
      </Route>
      <Redirect to="/not-found" />
    </Switch>
    // <Footer />
  );
};

export default Routes;
