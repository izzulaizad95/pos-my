import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "../../assets/styles/app.scss";
import Routes from "./Router/Routes";

// export const history = createBrowserHistory();

const App = () => {
  return (
    <Router>
      <Routes />
    </Router>
  );
};

export default App;
