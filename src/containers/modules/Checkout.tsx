import {
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { FloatButton } from "components/Button/FloatButton";
import React, { useEffect, useState } from "react";
import { useHistory, useLocation, useParams } from "react-router";
import { Header } from "../../components/Header/Header";

//import { useForm } from 'react-hook-form';

export const Checkout = (props: any) => {
  useEffect(() => {
    console.log("Checkout");
  }, []);
  const location = useLocation();
  const orderItemData = location?.state;
  const history = useHistory();
  const { mode } = useParams();

  /* -------------------------------------------- */
  /*                     STATE                    */
  /* -------------------------------------------- */
  let [orderItem, setOrderItem] = useState([]);

  /* -------------------------------------------- */
  /*                   USEEFFECT                  */
  /* -------------------------------------------- */
  useEffect(() => {
    if (orderItemData?.length) {
      setOrderItem(orderItemData);
    }
  }, [orderItemData]);

  /* -------------------------------------------- */
  /*                   FUNCTION                   */
  /* -------------------------------------------- */

  /* -------------------------------------------- */
  /*                  ITEM DIALOG                 */
  /* -------------------------------------------- */
  const onSubmit = () => {
    // say thanks and navigate to homepage
  };

  const itemTotal = orderItem?.reduce(
    (prevValue, currValue) => prevValue + currValue?.price,
    0
  );

  console.log("no change?", orderItemData);

  return (
    <>
      <Header
        title="POS"
        badgeCount={orderItem?.length}
        onClick={() => {
          history.push({
            pathname: `/${mode}/order`,
            state: orderItem,
          });
        }}
        rightInfo={`Total: RM${itemTotal}`}
      />
      <div
        className={"content-wrapper"}
        style={{ marginTop: "50px", padding: "0px" }}
      >
        <List className="core-list" style={{ padding: "0px" }}>
          {orderItem?.map((item, index) => {
            return (
              <>
                <ListItem>
                  <ListItemText
                    primary={
                      <>
                        <div className="flex-space">
                          <span className="smTitle">{item?.name}</span>
                          <span className="smTitle right">{`RM${item?.price}`}</span>
                        </div>
                      </>
                    }
                  />
                  <ListItemSecondaryAction
                    style={{ top: "4%", transform: "inherit" }}
                  >
                    <IconButton
                      edge="end"
                      aria-label="more"
                      aria-controls="menu-list"
                      aria-haspopup="true"
                      onClick={(e) => {
                        orderItemData.splice(index, 1);
                      }}
                    >
                      <DeleteIcon fontSize="small" />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
                <Divider />
              </>
            );
          })}
        </List>
      </div>

      {/* FLOAT BUTTON */}
      <FloatButton
        onClick={() => {
          history.push({
            pathname: `/${mode}/order`,
          });
        }}
        label="Cancel"
        style={{ borderRadius: "8px", right: "120px" }}
        color="secondary"
      />

      <FloatButton
        onClick={() => {
          history.push({
            pathname: `/${mode}/order/checkout`,
            state: orderItem,
          });
        }}
        label="Confirm"
        style={{ borderRadius: "8px" }}
      />
    </>
  );
};
