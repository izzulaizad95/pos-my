import {
  Badge,
  Divider,
  List,
  ListItem,
  ListItemText,
} from "@material-ui/core";
import { LocalMall } from "@material-ui/icons";
import { FloatButton } from "components/Button/FloatButton";
import { ItemOptionDialog } from "components/Dialog/ItemOptionDialog";
import React, { useEffect, useState } from "react";
import { useHistory, useLocation, useParams } from "react-router";
import { Header } from "../../components/Header/Header";

//import { useForm } from 'react-hook-form';

const menuItemList = [
  {
    name: "Chonky Chicken",
    price: 10,
  },
  {
    name: "Beef Barbeque",
    price: 12,
  },
  {
    name: "Hawking Hawaiian",
    price: 10,
  },
  {
    name: `Margaret's Margharita`,
    price: 8,
  },
  {
    name: "Vegan Villa Vista",
    price: 8,
  },
];

export const Order = (props: any) => {
  useEffect(() => {
    console.log("Order");
  }, []);
  const location = useLocation();
  const orderItemData = location?.state;
  const history = useHistory();
  const { mode } = useParams();

  /* -------------------------------------------- */
  /*                     STATE                    */
  /* -------------------------------------------- */
  const [selectedItem, setSelectedItem] = useState({
    name: "",
    price: 0,
  });
  const [openItemDialog, setOpenItemDialog] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const [cheese, setCheese] = useState(false);
  const [size, setSize] = useState("");
  const [orderItem, setOrderItem] = useState([]);

  /* -------------------------------------------- */
  /*                   USEEFFECT                  */
  /* -------------------------------------------- */

  useEffect(() => {
    if (orderItemData?.length > 0) {
      setOrderItem(orderItemData); // to set back order item from checkout page
    }
  }, [orderItemData]);

  /* -------------------------------------------- */
  /*                   FUNCTION                   */
  /* -------------------------------------------- */

  const clickItem = (item) => {
    setSelectedItem(item);
    setOpenItemDialog(true);
  };

  /* -------------------------------------------- */
  /*                  ITEM DIALOG                 */
  /* -------------------------------------------- */
  const onSubmit = () => {
    orderItem.push({
      name: selectedItem.name,
      basePrice: selectedItem.price,
      price: quantity * selectedItem.price,
      size: size,
      cheese: cheese,
      quantity: Number(quantity),
    });
    setOpenItemDialog(false);
  };

  /* -------------------------------------------- */
  /*                     MENU                     */
  /* -------------------------------------------- */

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Header
        title="POS"
        badgeCount={orderItem?.length}
        onClick={() => history.push("/home")}
        rightIcon={
          <Badge
            badgeContent={orderItem?.length}
            color="secondary"
            onClick={(e) => handleMenu(e)}
          >
            <LocalMall />
          </Badge>
        }
        rightMenu={true}
        menuData={orderItem}
        anchorEl={anchorEl}
        handleMenuClose={handleClose}
      />
      <div
        className={"content-wrapper"}
        style={{ marginTop: "50px", padding: "0px" }}
      >
        <List className="core-list" style={{ padding: "0px" }}>
          {menuItemList?.map((item, index) => {
            return (
              <>
                <ListItem onClick={() => clickItem(item)}>
                  <ListItemText
                    primary={
                      <>
                        <div className="flex-space">
                          <span className="smTitle">{item?.name}</span>
                          <span className="smTitle right">{`RM${item?.price}`}</span>
                        </div>
                      </>
                    }
                  />
                </ListItem>
                <Divider />
              </>
            );
          })}
        </List>
      </div>

      {/* ITEM DIALOG */}
      <ItemOptionDialog
        openItemDialog={openItemDialog}
        setOpenItemDialog={setOpenItemDialog}
        selectedItem={selectedItem}
        size={size}
        setSize={setSize}
        cheese={cheese}
        setCheese={setCheese}
        quantity={quantity}
        setQuantity={setQuantity}
        onSubmit={onSubmit}
      />

      {/* FLOAT BUTTON */}
      <FloatButton
        onClick={() => {
          history.push({
            pathname: `/${mode}/order/checkout`,
            state: orderItem,
          });
        }}
        label="Checkout"
        style={{ borderRadius: "8px" }}
      />
    </>
  );
};
