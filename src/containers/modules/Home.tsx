import { Grid } from "@material-ui/core";
import React, { useEffect } from "react";
import { useHistory, useLocation } from "react-router";

export const Home = () => {
  useEffect(() => {
    console.log("Home");
  }, []);
  let history = useHistory();
  let location = useLocation();

  const mainMenuList = [
    {
      path: `/order`,
      mode: `pick-up`,
      reroute: false,
      reroutePath: "",
      //   icon: OutletAppIcon,
      title: `Pick Up`,
    },
    {
      path: `/order`,
      mode: `delivery`,
      reroute: false,
      reroutePath: "",
      //   icon: KitchenAppIcon,
      title: `Delivery`,
    },
  ];

  useEffect(() => {
    localStorage.removeItem("mode");
  }, []);

  return (
    <>
      <div>
        <Grid container>
          {mainMenuList?.map((el, index) => {
            return (
              <Grid key={index} item xs={6} lg={3}>
                <div
                  onClick={() => {
                    localStorage.setItem("mode", el?.mode);
                    history.push(`/${el?.mode}/order`);
                  }}
                  className="xxTitle mobile-app-label"
                >
                  {el.title}
                </div>
              </Grid>
            );
          })}
        </Grid>
      </div>
    </>
  );
};
