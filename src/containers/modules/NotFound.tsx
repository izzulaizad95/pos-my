import React from "react";
import { useHistory } from "react-router";
import logo from "../../assets/logo.svg";

const NotFound = () => {
  let history = useHistory();

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Page not found!.</p>
        <div onClick={() => history.goBack()}>Please go back</div>
      </header>
    </div>
  );
};

export default NotFound;
